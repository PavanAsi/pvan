CREATE TABLE `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(1000) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `salary` int(5) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


INSERT INTO User (id, username, password, salary, age) VALUES (1, 'Alex123', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 3456, 33);
INSERT INTO User (id, username, password, salary, age) VALUES (2, 'Tom234', '$2a$04$PCIX2hYrve38M7eOcqAbCO9UqjYg7gfFNpKsinAxh99nms9e.8HwK', 7823, 23);
INSERT INTO User (id, username, password, salary, age) VALUES (3, 'Adam', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 4234, 45);

------------------------------------------------------------

http://localhost:8080/token/generate-token
post

{
	"username":"Alex123",
	"password":"password"
}

response:  {
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJBbGV4MTIzIiwic2NvcGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJpc3MiOiJodHRwOi8vZGV2Z2xhbi5jb20iLCJpYXQiOjE1MzMwNTg1NTEsImV4cCI6MTUzMzIzODU1MX0.4qpAerEZWwvZhEiUqhEhLaMPYV5A98uRNiiSq4NnzR8"
}


----------------------------------------------------
URL:   http://localhost:8080/signup
Authorization: Bearer  genarated token pass
accept:app/json
Content:app/json

{
	"username":"adishakar",
	"password":"adishakar",
	"age":27,
	"salary":1000
}
-------------------------------------------
http://localhost:8080/users
Authorization: Bearer  genarated token pass


respose: [
    {
        "id": 1,
        "username": "Alex123",
        "salary": 3456,
        "age": 33
    },
    {
        "id": 2,
        "username": "Tom234",
        "salary": 7823,
        "age": 23
    },
    {
        "id": 3,
        "username": "Adam",
        "salary": 4234,
        "age": 45
    }
]
--------------------------------------------------------------------------------



